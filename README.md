# go.php

#### 项目介绍
或是出于优化 SEO，或是出于加强网站体验，很多博客都给文章中的外部链接加上了个二次跳转，基本格式就是 

自己的网址/go.php?url=目标网址 或 自己的网址/go/?url=目标网址

如：https://www.tiezi.xyz/go/?url=www.xiaomanyao.org


这么做其实有个很大的安全隐患：

假设有个不怀好意的坏蛋，要传播一个不怀好意的网站 www.baidu.com (这里用百度做示范好了。。)，通常这类网站一发到 QQ 里 QQ 就会有一个大大的红色叹号以示危险，如果利用像本站的跳转功能，就能轻易地将链接“洗白”，也就是我们常说的防红链接

所以本文件的目的就是给跳转文件加一个判断，如果是在本站内直接访问目标网址，则进行无感跳转（直接跳转）；如果不是在本站内访问的目标网站，则弹出提示。


在线体验：

https://www.tiezi.xyz/archives/1214.html

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)